#! /usr/bin/env python

import rospy

from geometry_msgs.msg import Twist #import geometry_msgs for velocity and orientatio
from sensor_msgs.msg import LaserScan   #import sensor_msgs for LaserScan values

robot_vel = Twist() #define instance for Twist
robot_vel.linear.x = 0.5    #define an initial linear velocity
robot_vel.angular.z = 0.0   #define an initial angular rotation

def callback(msg):

    #for detecting obstacles within 1 m range
    if(msg.ranges[360]<1):
        robot_vel.linear.x = 0.1
        robot_vel.angular.z = -0.2

    #buggy if condition
    #if(msg.ranges[719] == float('inf') and msg.ranges[360]!=float('inf')):
        #robot_vel.linear.x = 0.0
        #robot_vel.angular.z = -0.1

    #for left turns, to fine tune the turn
    if(msg.ranges[0]!=float('inf')):
        robot_vel.linear.x = 0.0
        robot_vel.angular.z = -0.7

    #once clear of obstacle, move ahead, full steam!
    if(msg.ranges[360] == float('inf') and msg.ranges[719] == float('inf')):
        robot_vel.linear.x = 0.5
        robot_vel.angular.z = 0.0

    #for right turn
    if(msg.ranges[719]<1):
        robot_vel.linear.x = 0.0
        robot_vel.angular.z = 0.1

    #move_pub.publish(robot_vel)


rospy.init_node('topics_quiz_node') #initialise node
move_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1) #publisher
read_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, callback)  #subscriber

rate = rospy.Rate(2) #define rate = 2 Hz

#publish-evaluate conditions-update-repeat
while not rospy.is_shutdown():
    move_pub.publish(robot_vel)
    rate.sleep()