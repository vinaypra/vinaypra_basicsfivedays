#! /usr/bin/env python

import rospy    #importing rospy
from age_pkg.msg import Age #importing the message 'Age' from 'age_pkg' package

def main():
    rospy.init_node("age_publisher") #initialise node by the name 'age_publisher'
    pub = rospy.Publisher("Age", Age, queue_size=1) #define name of message, and message type as 'Age'
    r = rospy.Rate(2) #define rate to 2Hz

    age_var = Age() #create instance of Age as 'age_var'

    while not rospy.is_shutdown():
        age_var.years = 15  #initialise value for years
        age_var.months = 2  #initialise value for months
        age_var.days = 12   #initialise value for days

        pub.publish(age_var)    #publish the message
        r.sleep()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
