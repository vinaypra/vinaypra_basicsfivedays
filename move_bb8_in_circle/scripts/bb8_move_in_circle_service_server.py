#! /usr/bin/env python

import rospy
from std_srvs.srv import Empty, EmptyResponse # you import the service message python classes generated from Empty.srv.
from geometry_msgs.msg import Twist # for access to linear and angular speed attributes

def my_callback(request):
    bb8_circle.linear.x = 0.5
    bb8_circle.angular.z = 0.5
    bb8_circle_pub.publish(bb8_circle) # publish linear and angular speed values
    return EmptyResponse() # the service Response class, in this case EmptyResponse

bb8_circle = Twist() # create instance

rospy.init_node('move_bb8_circle_service_server')
my_service = rospy.Service('/move_bb8_in_circle', Empty , my_callback) # create the Service called my_service with the defined callback
bb8_circle_pub = rospy.Publisher('/cmd_vel', Twist, queue_size = 1) # publsiher for speed values

rospy.spin() # maintain the service open.