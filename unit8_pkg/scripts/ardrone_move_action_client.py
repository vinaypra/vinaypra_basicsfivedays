#! /usr/bin/env python

import rospy
import time
import actionlib
from ardrone_as.msg import ArdroneAction, ArdroneGoal, ArdroneResult, ArdroneFeedback
from std_msgs.msg import Empty
from geometry_msgs.msg import Twist

"""
class SimpleGoalState:
    PENDING = 0
    ACTIVE = 1
    DONE = 2
    WARN = 3
    ERROR = 4

"""
# We create some constants with the corresponing vaules from the SimpleGoalState class
PENDING = 0
ACTIVE = 1
DONE = 2
WARN = 3
ERROR = 4

nImage = 1

# definition of the feedback callback. This will be called when feedback
# is received from the action server
# it just prints a message indicating a new message has been received
def feedback_callback(feedback):
    """
    Error that might jump
    
    self._feedback.lastImage = 
AttributeError: 'ArdroneAS' obj
    
    """
    global nImage
    print('[Feedback] image n.%d received'%nImage)
    nImage += 1

# initializes the action client node
rospy.init_node('ardrone_move_around_action_client_node_ex')

action_server_name = '/ardrone_action_server'
client = actionlib.SimpleActionClient(action_server_name, ArdroneAction)


# Create message instance in Twist() for when moving the drone
ardrone_move_obj = Twist() 

# Publisher to publish velocity values for the ardrone
ardrone_move = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

# Create message instance in Empty() for ardrone takeoff maneuver
ardrone_takeoff_obj = Empty()

# Publisher to make the ardrone take-off
ardrone_takeoff = rospy.Publisher('/drone/takeoff', Empty, queue_size=1)

# Create message instance in Empty() for ardrone landing maneuver
ardrone_land_obj = Empty()

# Publisher to make the ardrone land back
ardrone_land = rospy.Publisher('/drone/land', Empty, queue_size=1)


# waits until the action server is up and running
rospy.loginfo('Waiting for action Server '+action_server_name)
client.wait_for_server()
rospy.loginfo('Action Server Found...'+action_server_name)

# creates a goal to send to the action server
goal = ArdroneGoal()
goal.nseconds = 10 # indicates, take pictures along 10 seconds

client.send_goal(goal, feedback_cb=feedback_callback)


# You can access the SimpleAction Variable "simple_state", that will be 1 if active, and 2 when finished.
#Its a variable, better use a function like get_state.
#state = client.simple_state
# state_result will give the FINAL STATE. Will be 1 when Active, and 2 if NO ERROR, 3 If Any Warning, and 3 if ERROR
state_result = client.get_state()

rate = rospy.Rate(1) #1 Hz

rospy.loginfo("state_result: "+str(state_result))

# Getting the ardrone airborne initially

# for loop approach
for x in range(6):
    ardrone_takeoff.publish(ardrone_takeoff_obj)
    time.sleep(1)
    
'''
# while loop approach
x = 0
while not x == 5:
    ardrone_takeoff.publish(ardrone_takeoff_obj)
    time.sleep(1)
    x += 1
'''

# Moving the drone while images are being captured    
while state_result < DONE:
    rospy.loginfo("Doing Stuff while waiting for the Server to give a result....")
    
    # Setting velocity values for moving the ardrone in air
    ardrone_move_obj.linear.x = 0.5
    ardrone_move_obj.angular.z = 0.5

    # Publish the ardrone velocities
    ardrone_move.publish(ardrone_move_obj)

    rate.sleep()
    state_result = client.get_state()
    rospy.loginfo("state_result: "+str(state_result))
    
rospy.loginfo("[Result] State: "+str(state_result))
if state_result == ERROR:
    rospy.logerr("Something went wrong in the Server Side")
if state_result == WARN:
    rospy.logwarn("There is a warning in the Server Side")

#rospy.loginfo("[Result] State: "+str(client.get_result()))

# for loop approach
for y in range(6):
    # Stop the movement of the drone altogether
    ardrone_move_obj.linear.x = 0.0 # Set linear velocity zero
    ardrone_move_obj.angular.z = 0.0   # Set angular velocity zero
    ardrone_move.publish(ardrone_move_obj)  # Publish the values

    ardrone_land.publish(ardrone_land_obj)  # Publish land maneuver
    time.sleep(1)

'''
# while loop approach
x = 0
while not x == 5:
    # Stop the movement of the drone altogether
    ardrone_move_obj.linear.x = 0 # Set linear velocity zero
    ardrone_move_obj.angular.z = 0   # Set angular velocity zero
    ardrone_move.publish(ardrone_move_obj)  # Publish the values

    ardrone_land.publish(ardrone_land_obj)  # Publish land maneuver
    time.sleep(1)
    x += 1
'''