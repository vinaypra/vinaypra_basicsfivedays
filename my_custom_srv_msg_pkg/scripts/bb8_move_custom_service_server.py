#! /usr/bin/env python

import rospy
#from std_srvs.srv import Empty, EmptyResponse # you import the service message python classes generated from Empty.srv.
from my_custom_srv_msg_pkg.srv import MyCustomServiceMessage, MyCustomServiceMessageResponse # you import the service message python classes 
                                                                                         # generated from MyCustomServiceMessage.srv.

from geometry_msgs.msg import Twist # for access to linear and angular speed attributes

def my_callback(request):

    #my_response = MyCustomServiceMessageResponse()
    time_dur = MyCustomServiceMessage()
    time_counter = time_dur.duration
    #time_counter = MyCustomServiceMessage.duration

    # Now setting values to move BB8 in a circle when in the specified duration
    while t>0:
        # Circular trajectory movement values set here
        bb8_circle.linear.x = 0.5
        bb8_circle.angular.z = 0.5
        bb8_circle_pub.publish(bb8_circle) # publish linear and angular speed values

        # Decreasing counter t
        time_counter = time_counter-1
        rospy.sleep(1)
    
    # Now setting values to stop BB8 outside of the specified duration
    bb8_circle.linear.x = 0.0
    bb8_circle.angular.z = 0.0
    bb8_circle_pub.publish(bb8_circle) # publish linear and angular speed values
    
    #my_response.success = True
    #return my_response # the service Response class, in this case EmptyResponse
    MyCustomServiceMessage.success = True
    return MyCustomServiceMessageResponse()

bb8_circle = Twist() # create instance

rospy.init_node('move_bb8_circle_custom_service_server')
my_service = rospy.Service('/move_bb8_in_circle_custom', MyCustomServiceMessage, my_callback) # create the Service called my_service with the defined callback
bb8_circle_pub = rospy.Publisher('/cmd_vel', Twist, queue_size = 1) # publsiher for speed values

rospy.spin() # maintain the service open.