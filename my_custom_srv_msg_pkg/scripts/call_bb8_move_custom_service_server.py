#! /usr/bin/env python

# import packages
import rospkg
import rospy
import sys
# Import the service message used by the service /trajectory_by_name
from my_custom_srv_msg_pkg.srv import MyCustomServiceMessage, MyCustomServiceMessageRequest 

# Initialise a ROS node with the name service_client
rospy.init_node('move_bb8_circle_custom_service_client')
# Wait for the service client /trajectory_by_name to be running
rospy.wait_for_service('/move_bb8_in_circle_custom')
# Create the connection to the service
move_bb8_in_circle_custom_service = rospy.ServiceProxy('/move_bb8_in_circle_custom', MyCustomServiceMessage)
# Create an object of type TrajByNameRequest
move_bb8_in_circle_custom_object = MyCustomServiceMessageRequest ()
# Fill the variable traj_name of this object with the desired value
move_bb8_in_circle_custom_object.duration = 15
# Send through the connection the name of the trajectory to be executed by the robot
result = move_bb8_in_circle_custom_service(move_bb8_in_circle_custom_object)
# Print the result given by the service called
print (result)