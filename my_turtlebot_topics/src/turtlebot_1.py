#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
import time

rospy.init_node('auton_turtlebot')
#sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, callback)
pub_1 = rospy.Publisher('/cmd_vel', Twist, queue_size=1) 
pub_2 = rospy.Publisher('/cmd_vel', Twist, queue_size=1) 

rate = rospy.Rate(1)   # Set a publish rate of 2 Hz

turtlebot_obj = Twist()
time_turn = 2.0
time_sleep = 10.0
#i = 0
def rotate_turtlebot():
    #i=0
    turtlebot_obj.linear.x = 0.0
    turtlebot_obj.angular.z = 0.8
    pub_1.publish(turtlebot_obj)
    #pub.publish(turtlebot_obj)
    #turtlebot_obj.linear.x = 0.0
    #turtlebot_obj.angular.z = 0.0
    time.sleep(time_turn)

def stop_turtlebot():
    turtlebot_obj.linear.x = 0.0
    turtlebot_obj.angular.z = 0.0
    #pub.publish(turtlebot_obj)
    pub_2.publish(turtlebot_obj)
    time.sleep(time_sleep)

def callback(msg):

    # Left direction - 0:100
    # Straight direction - 320:400
    # Right direction - 619:719

    left_dir = msg.ranges[:100]
    #laser_scan_values.left_dir_val = min(left_dir)
    left_dir_val = min(left_dir)
    rospy.loginfo("left.....")

    straight_dir = msg.ranges[320:400]
    #laser_scan_values.straight_dir_val = min(straight_dir)
    straight_dir_val = min(straight_dir)
    rospy.loginfo(".......straight.....")

    right_dir = msg.ranges[619:]
    #laser_scan_values.right_dir_val = min(right_dir)
    right_dir_val = min(right_dir)
    rospy.loginfo("......................right")

    return left_dir_val, straight_dir_val, right_dir_val



while not rospy.is_shutdown():
    laser_value_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, callback)

    #i = 0
    #rotate_turtlebot()
    #pub.publish(turtlebot_obj)
    #stop_turtlebot()
    
    left_lv, straight_lv, right_lv = callback()
    print(left_lv)
    print(straight_lv)
    print(right_lv)

    '''
    turtlebot_obj.linear.x = 0.0
    turtlebot_obj.angular.z = 0.0
    pub.publish(turtlebot_obj)

    time.sleep(time_sleep)
    '''

    #rate.sleep()
#rospy.spin()