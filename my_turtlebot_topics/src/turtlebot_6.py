#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
import time


turtlebot_obj = Twist()
# Inital values for velocity
turtlebot_obj.linear.x = 0.5
# Clockwise - positive; Anti Clockwise - negative
turtlebot_obj.angular.z = 0.0

time_turn = 2.0

def callback(msg):
    left_lv = msg.ranges[719]
    straight_lv = msg.ranges[360]
    right_lv = msg.ranges[0]

    if straight_lv>=1:
        # Move straight
        turtlebot_obj.linear.x = 0.8
        turtlebot_obj.linear.y = 0.0
        turtlebot_obj.angular.z = 0.0
        #move_straight()

    elif straight_lv<1:
        turtlebot_obj.linear.x = 0.0
        turtlebot_obj.angular.z = 0.0
        #stop_robot()

        if right_lv > 1 and left_lv < 1:
            # Turn right by 90 degrees
            turtlebot_obj.linear.x = 0.2
            turtlebot_obj.angular.z = 1.0 #0.8
            #time.sleep(time_turn)
            #right_turn()

        if left_lv > 1 and right_lv < 1:
            # Turn left by 90 degrees
            turtlebot_obj.linear.x = 0.2
            turtlebot_obj.angular.z = -1.0 #0.8
            #time.sleep(time_turn)
            #right_turn()

        


rospy.init_node('maze_turtlebot')
move_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1) #publisher
read_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, callback)

rate = rospy.Rate(2) #define rate = 2 Hz

while not rospy.is_shutdown():
    move_pub.publish(turtlebot_obj)
    rate.sleep()