#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
import time


turtlebot_obj = Twist()
# Inital values for velocity
turtlebot_obj.linear.x = 0.5
# Clockwise - positive; Anti Clockwise - negative
turtlebot_obj.angular.z = 0.0

time_turn = 2.0

def callback(msg):
    left_lv = msg.ranges[719]
    straight_lv = msg.ranges[360]
    right_lv = msg.ranges[0]

    #if straight_lv >= 0.3 or left_lv >= 0.3 or right_lv >= 0.3:
    if straight_lv > left_lv and straight_lv > right_lv:
        # Move straight
        turtlebot_obj.linear.x = 0.8
        turtlebot_obj.angular.z = 0.0
        #move_straight()

        if left_lv > right_lv:
                # Move straight and angle left slightly
                turtlebot_obj.linear.x = 0.8
                turtlebot_obj.angular.z = -0.1
                #move_straight_light_left()

        if right_lv > left_lv:
                # Move straight and angle right slightly
                turtlebot_obj.linear.x = 0.8
                turtlebot_obj.angular.z = 0.1
                #move_straight_light_right()

    if left_lv <= 0.3 and straight_lv >= 0.3:
        # Move straight and angle right sharply
        turtlebot_obj.linear.x = 0.2
        turtlebot_obj.angular.z = 0.5
        #move_straight_hard_right()

    if right_lv <= 0.3 and straight_lv >= 0.3:
        # Move straight and angle left sharply
        turtlebot_obj.linear.x = 0.2
        turtlebot_obj.angular.z = -0.5
        #move_straight_hard_left()

    if straight_lv <= 0.3:
        # Stop the robot immediately
        turtlebot_obj.linear.x = 0.0
        turtlebot_obj.angular.z = 0.0
        #stop_robot()

        if left_lv > straight_lv and left_lv > right_lv:
            # Turn left by 90 degrees
            turtlebot_obj.linear.x = 0.0
            turtlebot_obj.angular.z = -0.8
            time.sleep(time_turn)
            #left_turn()

        if right_lv > straight_lv and right_lv > left_lv:
            # Turn right by 90 degrees
            turtlebot_obj.linear.x = 0.0
            turtlebot_obj.angular.z = 0.8
            time.sleep(time_turn)
            #right_turn()

        turtlebot_obj.linear.x = 0.8
        turtlebot_obj.angular.z = 0.0

    if straight_lv == float('inf') or left_lv == float('inf') or right_lv == float('inf'):
        turtlebot_obj.linear.x = 0.0
        turtlebot_obj.angular.z = 0.0
        #stop_robot()

        if left_lv == float('inf'):
            turtlebot_obj.linear.x = 0.0
            turtlebot_obj.angular.z = -0.8
            time.sleep(time_turn)
            #left_turn()
            turtlebot_obj.linear.x = 0.8
            turtlebot_obj.angular.z = 0.0
            #move_straight()

        if right_lv == float('inf'):
            turtlebot_obj.linear.x = 0.0
            turtlebot_obj.angular.z = 0.8
            time.sleep(time_turn)
            #right_turn()
            turtlebot_obj.linear.x = 0.8
            turtlebot_obj.angular.z = 0.0
            #move_straight()

        if straight_lv == float('inf'):
            turtlebot_obj.linear.x = 0.8
            turtlebot_obj.angular.z = 0.0
            #move_straight()

    if left_lv == float('inf') and straight_lv == float('inf') and right_lv == float('inf'):
        turtlebot_obj.linear.x = 0.0
        turtlebot_obj.angular.z = 0.0
        #stop_robot()

rospy.init_node('maze_turtlebot')
move_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1) #publisher
read_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, callback)

rate = rospy.Rate(2) #define rate = 2 Hz

while not rospy.is_shutdown():
    move_pub.publish(turtlebot_obj)
    rate.sleep()