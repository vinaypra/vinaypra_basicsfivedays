#! /usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
import time

rospy.init_node('maze_turtlebot')

#laser_value_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, laser_scan_values)

move_straight_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

move_straight_light_left_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
move_straight_light_right_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

move_straight_hard_left_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
move_straight_hard_right_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

rotate_right_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1) 
rotate_left_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1) 

stop_robot_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1) 

turtlebot_obj = Twist()
# Inital values for velocity
turtlebot_obj.linear.x = 0.0
# Clockwise - positive; Anti Clockwise - negative
turtlebot_obj.angular.z = 0.0

time_turn = 2.0 # Finetuned for angular.z=0.8 for a 90 degree turn

def laser_scan_values(msg):

    # Left direction - 0:100
    # Straight direction - 320:400
    # Right direction - 619:719

    left_dir = msg.ranges[0:100]
    laser_scan_values.left_dir_val = min(left_dir)

    straight_dir = msg.ranges[320:400]
    laser_scan_values.straight_dir_val = min(straight_dir)

    right_dir = msg.ranges[619:719]
    laser_scan_values.right_dir_val = min(right_dir)

    return (left_dir_val, straight_dir_val, right_dir_val)

def move_straight():
    turtlebot_obj.linear.x = 0.8
    turtlebot_obj.linear.y = 0.0
    turtlebot_obj.angular.z = 0.0
    move_straight_pub.publish(turtlebot_obj)

def move_straight_light_left():
    turtlebot_obj.linear.x = 0.8
    #turtlebot_obj.linear.y = 0.0
    turtlebot_obj.angular.z = -0.1
    move_straight_light_left_pub.publish(turtlebot_obj)

def move_straight_light_right():
    turtlebot_obj.linear.x = 0.8
    #turtlebot_obj.linear.y = 0.0
    turtlebot_obj.angular.z = 0.1
    move_straight_light_right_pub.publish(turtlebot_obj)

def move_straight_hard_left():
    turtlebot_obj.linear.x = 0.2
    turtlebot_obj.angular.z = -0.5
    move_straight_hard_left_pub.publish(turtlebot_obj)

def move_straight_hard_right():
    turtlebot_obj.linear.x = 0.2
    turtlebot_obj.angular.z = 0.5
    move_straight_hard_right_pub.publish(turtlebot_obj)

def right_turn():
    turtlebot_obj.linear.x = 0.0
    turtlebot_obj.angular.z = 0.8
    rotate_right_pub.publish(turtlebot_obj)
    time.sleep(time_turn)
    stop_robot()

def left_turn():
    turtlebot_obj.linear.x = 0.0
    turtlebot_obj.angular.z = -0.8
    rotate_left_pub.publish(turtlebot_obj)
    time.sleep(time_turn)
    stop_robot()

def stop_robot():
    turtlebot_obj.linear.x = 0.0
    turtlebot_obj.angular.z = 0.0
    stop_robot_pub.publish(turtlebot_obj)

def move_decide():
    
    laser_scan_values()
    
    left_lv = laser_scan_values.left_dir_val
    straight_lv = laser_scan_values.straight_dir_val
    right_lv = laser_scan_values.right_dir_val

    while straight_lv >= 0.3 or left_lv >= 0.3 or right_lv >= 0.3:

        if straight_lv > left_lv and straight_lv > right_lv:
            # Move straight
            #turtlebot_obj.linear.x = 0.8
            #turtlebot_obj.angular.z = 0.0
            move_straight()

            if left_lv > right_lv:
                # Move straight and angle left slightly
                #turtlebot_obj.linear.x = 0.8
                #turtlebot_obj.angular.z = -0.1
                move_straight_light_left()

            if right_lv > left_lv:
                # Move straight and angle right slightly
                #turtlebot_obj.linear.x = 0.8
                #turtlebot_obj.angular.z = 0.1
                move_straight_light_right()

    while left_lv <= 0.3 and straight_lv >= 0.3:
        # Move straight and angle right sharply
        #turtlebot_obj.linear.x = 0.2
        #turtlebot_obj.angular.z = 0.5
        move_straight_hard_right()

    while right_lv <= 0.3 and straight_lv >= 0.3:
        # Move straight and angle left sharply
        #turtlebot_obj.linear.x = 0.2
        #turtlebot_obj.angular.z = -0.5
        move_straight_hard_left()

    while straight_lv <= 0.3:
        # Stop the robot immediately
        stop_robot()
        
        # Get laser values again
        laser_scan_values()
    
        left_lv = laser_scan_values.left_dir_val
        straight_lv = laser_scan_values.straight_dir_val
        right_lv = laser_scan_values.right_dir_val

        if left_lv > straight_lv and left_lv > right_lv:
            # Turn left by 90 degrees
            left_turn()

        if right_lv > straight_lv and right_lv > left_lv:
            # Turn right by 90 degrees
            right_turn()

        move_straight()

    if straight_lv == float('inf') or left_lv == float('inf') or right_lv == float('inf'):
        stop_robot()

        # Get laser values again
        laser_scan_values()
    
        left_lv = laser_scan_values.left_dir_val
        straight_lv = laser_scan_values.straight_dir_val
        right_lv = laser_scan_values.right_dir_val

        if left_lv == float('inf'):
            left_turn()
            move_straight()

        if right_lv == float('inf'):
            right_turn()
            move_straight()

        if straight_lv == float('inf'):
            move_straight()

    if left_lv == float('inf') and straight_lv == float('inf') and right_lv == float('inf'):
        stop_robot()

#laser_value_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, laser_scan_values)

while not rospy.is_shutdown():
    #i = 0
    #laser_value_sub = rospy.Subscriber('/kobuki/laser/scan', LaserScan, laser_scan_values)
    move_decide()           
