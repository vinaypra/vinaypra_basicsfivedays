#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty as Empty
from std_srvs.srv import Empty as Empty_srv
from std_srvs.srv import EmptyResponse
from geometry_msgs.msg import Twist

def serv_callback(req):

    response = EmptyResponse()

    drone_takeoff_pub.publish(Empty())
    rospy.sleep(2)

    drone_move_forward()
    rospy.sleep(5)

    drone_stop()
    rospy.sleep(2)

    drone_land_pub.publish(Empty())
    #rospy.sleep(2)

    return response


def drone_move_forward():

    drone_move_obj.linear.x = 1.0
    drone_move_obj.linear.y = 0.0
    drone_move_obj.linear.z = 0.0
    drone_move_obj.angular.z = 0.0

    drone_move_pub.publish(drone_move_obj)

def drone_stop():

    drone_move_obj.linear.x = 0.0
    drone_move_obj.linear.y = 0.0
    drone_move_obj.linear.z = 0.0
    drone_move_obj.angular.z = 0.0

    drone_move_pub.publish(drone_move_obj)

rospy.init_node("motion_service_node")
drone_move_obj = Twist()
rate = rospy.Rate(1)

my_service_service = rospy.Service("my_service", Empty_srv, serv_callback)
rospy.sleep(1)

drone_takeoff_pub = rospy.Publisher("/drone/takeoff", Empty, queue_size=1)
rospy.sleep(1)

drone_move_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
rospy.sleep(1)

drone_land_pub = rospy.Publisher("/drone/land", Empty, queue_size=1)
rospy.sleep(1)

'''
drone_takeoff_pub.publish(Empty())
rospy.sleep(2)

drone_move_forward()
rospy.sleep(5)

drone_stop()
rospy.sleep(2)

drone_land_pub.publish(Empty())
rospy.sleep(2)
'''
while not rospy.is_shutdown():
    connections = drone_takeoff_pub.get_num_connections()
    if connections > 0:
        #my_service_service = rospy.Service("my_service", Empty_srv, serv_callback)
        '''
        drone_takeoff_pub = rospy.Publisher("/drone/takeoff", Empty, queue_size=1)

        drone_move_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)

        drone_land_pub = rospy.Publisher("/drone/land", Empty, queue_size=1)
        
        break
        '''
    else:
        rate.sleep()
