#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty
from std_srvs.srv import Trigger, TriggerRequest
from geometry_msgs.msg import Twist, Pose
import actionlib
from path_exam.msg import RecordOdomAction, RecordOdomFeedback, RecordOdomResult, RecordOdomGoal

def feedback_callback(feedback):
            pass

def main():
    try:

        rospy.init_node("main_program_node")
        
        rospy.wait_for_service("/my_service")
        my_service_service = rospy.ServiceProxy("/my_service", Trigger)
        my_service_obj = TriggerRequest()
        result_1 = my_service_service(my_service_obj)
        print result_1

        client = actionlib.SimpleActionClient("rec_pose_as", RecordOdomAction)
        client.wait_for_server()

        goal = RecordOdomGoal()

        client.send_goal(goal, feedback_cb=feedback_callback)

        client.wait_for_result()
        action_result = client.get_result()


        print("Last Pose:")
        print action_result.trajectory_arr[-1]


    except rospy.ROSInterruptException:
        rospy.loginfo('Something went wrong!')
        #pass

if __name__ == "__main__":
    main()