#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty
from geometry_msgs.msg import Twist

rospy.init_node("forward_motion_node")
drone_move_obj = Twist()
#rate = rospy.Rate(1)

def drone_move_forward():

    drone_move_obj.linear.x = 1.0
    drone_move_obj.linear.y = 0.0
    drone_move_obj.linear.z = 0.0
    drone_move_obj.angular.z = 0.0

    drone_move_pub.publish(drone_move_obj)

def drone_stop():

    drone_move_obj.linear.x = 0.0
    drone_move_obj.linear.y = 0.0
    drone_move_obj.linear.z = 0.0
    drone_move_obj.angular.z = 0.0

    drone_move_pub.publish(drone_move_obj)

#rospy.loginfo('1')
drone_takeoff_pub = rospy.Publisher("/drone/takeoff", Empty, queue_size=1)
rospy.sleep(1)
#rospy.loginfo('2')
drone_move_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
rospy.sleep(1)
#rospy.loginfo('3')
drone_land_pub = rospy.Publisher("/drone/land", Empty, queue_size=1)
rospy.sleep(1)
#rospy.loginfo('4')

drone_takeoff_pub.publish(Empty())
rospy.sleep(2)
#rospy.loginfo('5')
drone_move_forward()
rospy.sleep(5)
#rospy.loginfo('6')
drone_stop()
rospy.sleep(2)

drone_land_pub.publish(Empty())
#rospy.sleep(2)

'''
connections = drone_takeoff_pub.get_num_connections()
if connections > 0:

    drone_takeoff_pub.publish(Empty())
    rospy.sleep(1)

    drone_move_forward()
    rospy.sleep(5)

    drone_stop()
    rospy.sleep(1)

    drone_land_pub.publish(Empty())
    rospy.sleep(1)

    #break
else:
    rospy.sleep(1)
'''
'''
while not rospy.is_shutdown():
    connections = drone_takeoff_pub.get_num_connections()
    if connections > 0:

        drone_takeoff_pub.publish(Empty())
        rospy.sleep(2)

        drone_move_forward()
        rospy.sleep(5)

        drone_stop()
        rospy.sleep(2)

        drone_land_pub.publish(Empty())
        rospy.sleep(2)

        break
    else:
        rate.sleep()
'''
