#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty as Empty
from std_srvs.srv import Trigger, TriggerResponse
from geometry_msgs.msg import Twist, Pose
import actionlib
from path_exam.msg import RecordOdomResult, RecordOdomFeedback, RecordOdomAction

class ParrotARDroneClass:

    #_feedback = RecordOdomFeedback()
    #_result = RecordOdomResult()

    def __init__(self):
        self._feedback = RecordOdomFeedback()
        self._result = RecordOdomResult()

        my_service_service = rospy.Service("my_service", Trigger, self.serv_callback)

        self._as = actionlib.SimpleActionServer("/rec_pose_as", RecordOdomAction, self.as_callback, False)
        self._as.start() 

        self.drone_move_obj = Twist()
        self.rate = rospy.Rate(1)

        self.drone_takeoff_pub = rospy.Publisher("/drone/takeoff", Empty, queue_size=1)
        #rospy.sleep(1)
        self.drone_move_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
        #rospy.sleep(1)
        self.drone_land_pub = rospy.Publisher("/drone/land", Empty, queue_size=1)
        #rospy.sleep(1)
        '''
        while not rospy.is_shutdown():
            self.connections = self.drone_takeoff_pub.get_num_connections()

            if self.connections > 0:
        '''
                #break
        '''
            else:
                self.rate.sleep()
        '''

    def serv_callback(self, req):
        self.response = TriggerResponse()
        #self.distance = 0.0
        #self.response.success = False # For debugging

        self.drone_pose_sub = rospy.Subscriber("/drone/gt_pose", Pose, self.pose_callback)
        self.pose_var = Pose()
        rospy.sleep(1)

        self.drone_takeoff_pub.publish(Empty())
        rospy.sleep(2)

        self.drone_start_pose = self.pose_var
        #self.drone_start_pose = self.drone_pose()
        rospy.sleep(0.5)

        self.drone_move_forward()
        rospy.sleep(5)

        self.drone_stop()
        rospy.sleep(2)

        self.drone_last_pose = self.pose_var
        #self.drone_last_pose = self.drone_pose()
        #self.rate.sleep()

        self.distance = self.drone_last_pose.position.x - self.drone_start_pose.position.x

        self.drone_land_pub.publish(Empty())
        #rospy.sleep(2)

        if self.distance != 0:
            self.response.message = ("The drone has moved %f meteres" %self.distance)
            self.response.success = True

        else:
            self.distance = 0.0
            self.response.message = ("The drone has not moved, distance is %f meteres" %self.distance)
            self.response.success = False

        return self.response


    def pose_callback(self, msg):

        self.pose_var = msg
        rospy.sleep(1)
    '''
    def drone_pose(self):

        return self.pose_var
    '''
    def drone_move_forward(self):

        self.drone_move_obj.linear.x = 1.0
        self.drone_move_obj.linear.y = 0.0
        self.drone_move_obj.linear.z = 0.0
        self.drone_move_obj.angular.z = 0.0

        self.drone_move_pub.publish(self.drone_move_obj)

    def drone_stop(self):

        self.drone_move_obj.linear.x = 0.0
        self.drone_move_obj.linear.y = 0.0
        self.drone_move_obj.linear.z = 0.0
        self.drone_move_obj.angular.z = 0.0

        self.drone_move_pub.publish(self.drone_move_obj)

    def as_callback(self, goal):
        self.pose_var = Pose()
        self.success = True
        '''
        if self._as.is_preempt_requested():
            rospy.loginfo('The goal has been cancelled/preempted')
            # the following line, sets the client in preempted state (goal cancelled)
            self._as.set_preempted()
            success = False
            break # Causing eror, buggy break
        '''

        self._result.trajectory_arr = []

        for i in range(0, 20):
            self.drone_loc_var = self.pose_var
            #self.drone_loc_var = self.drone_pose()
            self._result.trajectory_arr.append(self.drone_loc_var)
            rospy.sleep(1)
        
        self._feedback = Empty()

        if self.success:
            self._as.set_succeeded(self._result)
            print self._result


def main():
    try:

        rospy.init_node("check_distance_action_node")
        ParrotARDroneClass()
        rospy.spin()

    except rospy.ROSInterruptException:
        rospy.loginfo('Something went wrong!')
        #pass

if __name__ == "__main__":
    main()