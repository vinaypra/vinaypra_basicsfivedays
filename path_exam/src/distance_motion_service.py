#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty as Empty
from std_srvs.srv import Trigger, TriggerResponse
from geometry_msgs.msg import Twist, Pose

class ParrotARDroneClass:

    def __init__(self):

        my_service_service = rospy.Service("my_service", Trigger, self.serv_callback)
        #rospy.sleep(1)
        #self.rate = rospy.Rate(1)

        self.drone_takeoff_pub = rospy.Publisher("/drone/takeoff", Empty, queue_size=1)
        #rospy.sleep(1)
        self.drone_move_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
        #rospy.sleep(1)
        self.drone_move_obj = Twist()

        self.drone_land_pub = rospy.Publisher("/drone/land", Empty, queue_size=1)
        #rospy.sleep(1)
        '''
        while not rospy.is_shutdown():
            self.connections = self.drone_takeoff_pub.get_num_connections()

            if self.connections > 0:
        '''
                #break
        '''
            else:
                rospy.sleep(1)
        '''

    def serv_callback(self, req):
        self.response = TriggerResponse()
        #self.distance = 0.0
        #self.response.success = False # For debugging

        self.drone_pose_sub = rospy.Subscriber("/drone/gt_pose", Pose, self.pose_callback)
        self.pose_var = Pose()

        self.drone_takeoff_pub.publish(Empty())
        rospy.sleep(2)

        self.drone_start_pose = self.drone_xpose()
        rospy.sleep(1)

        self.drone_move_forward()
        rospy.sleep(5)

        self.drone_stop()
        rospy.sleep(2)

        self.drone_last_pose = self.drone_xpose()
        #rospy.sleep(1)

        self.distance = self.drone_last_pose - self.drone_start_pose

        self.drone_land_pub.publish(Empty())
        #rospy.sleep(2)

        if self.distance != 0:
            self.response.message = ("The drone has moved %f meters." %self.distance)
            self.response.success = True

        else:
            self.distance = 0.0
            self.response.message = ("The drone has not moved, distance is %f meteres" %self.distance)
            self.response.success = False # For debugging

        return self.response


    def pose_callback(self, msg):

        self.pose_var = msg


    def drone_xpose(self):

        return self.pose_var.position.x

    def drone_move_forward(self):

        self.drone_move_obj.linear.x = 1.0
        self.drone_move_obj.linear.y = 0.0
        self.drone_move_obj.linear.z = 0.0
        self.drone_move_obj.angular.z = 0.0

        self.drone_move_pub.publish(self.drone_move_obj)

    def drone_stop(self):

        self.drone_move_obj.linear.x = 0.0
        self.drone_move_obj.linear.y = 0.0
        self.drone_move_obj.linear.z = 0.0
        self.drone_move_obj.angular.z = 0.0
        rospy.sleep(2)
        self.drone_move_pub.publish(self.drone_move_obj)


def main():
    try:

        rospy.init_node("distance_motion_service_node")
        ParrotARDroneClass()
        rospy.spin()

    except rospy.ROSInterruptException:
        rospy.loginfo('Something went wrong!')
        #pass

if __name__ == "__main__":
    main()

