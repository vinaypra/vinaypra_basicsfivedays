#! /usr/bin/env python

import rospy
from std_msgs.msg import Empty

rospy.init_node("drone_takeoff_node")

drone_takeoff_pub = rospy.Publisher("/drone/takeoff", Empty, queue_size=1)
rate = rospy.Rate(1)

while not rospy.is_shutdown():
    connections = drone_takeoff_pub.get_num_connections()
    if connections > 0:
        drone_takeoff_pub.publish(Empty())
        break
    else:
        rate.sleep()