#! /usr/bin/env python

# Import the necessary packages
import rospy
from std_msgs.msg import Empty
from geometry_msgs.msg import Twist
from actionlib.msg import TestFeedback, TestResult, TestAction
import time
import actionlib

class ArdroneSquareMoveClass(object):

    # create messages that are used to publish feedback/result
    _feedback = TestFeedback()
    _result = TestResult()

    def __init__(self):
        # creates the action server
        self._as = actionlib.SimpleActionServer("ardrone_square_as", TestAction, self.goal_callback, False)
        self._as.start()
        self.ctrl_c = False
        self.rate = rospy.Rate(10)
    
    def goal_callback(self, goal):
        # this callback is called when the action server is called.
        # this is the function that computes the Fibonacci sequence
        # and returns the sequence to the node that called the action server
    
        # helper variables
        r = rospy.Rate(1)
        success = True
         
        # Create message instance in Twist() for when moving the ardrone
        self._ardrone_move = Twist()

        # Publisher to publish velocity values for the ardrone
        self._ardrone_move_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
        

        # Create message instance in Empty() for ardrone takeoff maneuver
        self._ardrone_takeoff_obj = Empty()

        # Publisher to make the ardrone take-off
        self._ardrone_takeoff_pub = rospy.Publisher('/drone/takeoff', Empty, queue_size=1)

        
        # Create message instance in Empty() for ardrone landing maneuver
        self._ardrone_land_obj = Empty()

        # Publisher to make the ardrone land
        self._ardrone_land_pub = rospy.Publisher('/drone/land', Empty, queue_size=1)
    
        # Getting the ardrone airborne initially
        # while loop approach
        x = 0
        while not x == 5 and not self.ctrl_c:
            self._ardrone_takeoff_pub.publish(self._ardrone_takeoff_obj)
            time.sleep(1)
            x += 1
    
        x = 0
        for x in range(5):
            # Push the value of side the ardron is on in the feedback
            # rospy.loginfo('We are at side number: ')
            self._feedback.feedback = x+1
            self._as.publish_feedback(self._feedback)
      
            # check that preempt (cancelation) has not been requested by the action client
            if self._as.is_preempt_requested():
                rospy.loginfo('The goal has been cancelled/preempted')
                # the following line, sets the client in preempted state (goal cancelled)
                self._as.set_preempted()
                success = False
                # we end the calculation of the Fibonacci sequence
                break

            time_side = goal.goal
            time_turn = 2.8 # finetuned for goal = 4
      
            # Set velocity values to move the ardrone straight
            self._ardrone_move.linear.x = 0.50
            self._ardrone_move.angular.z = 0.00
            self.publish_once(self._ardrone_move)
            time.sleep(time_side)
      
            # Set velocity values to turn the ardrone
            self._ardrone_move.linear.x = 0.00
            self._ardrone_move.angular.z = 0.50
            self.publish_once(self._ardrone_move)
            time.sleep(time_turn)
      
     
            # the sequence is computed at 1 Hz frequency
            r.sleep()
    
        # at this point, either the goal has been achieved (success==true)
        # or the client preempted the goal (success==false)
        # If success, then we publish the final result
        # If not success, we do not publish anything in the result
        if success:
            self._result.result = (time_turn*4)+(time_side*4)
            rospy.loginfo('Total number of seconds taken to execute the square = '+str(self._result.result))
            self._as.set_succeeded(self._result)
      
            # Set velocity values to stop the ardrone
            self._ardrone_move.linear.x = 0.0
            self._ardrone_move.angular.z = 0.0
            self.publish_once(self._ardrone_move)
      
            # Set velocity values to land the ardrone
            # while loop approach
            x = 0
            while not x == 5:
                self._ardrone_land_pub.publish(self._ardrone_land_obj)
                time.sleep(1)
                x += 1

    def publish_once(self, cmd):
        while not self.ctrl_c:
            connections = self._ardrone_move_pub.get_num_connections()
            if connections > 0:
                self._ardrone_move_pub.publish(cmd)
                break
            else:
                self.rate.sleep()

if __name__ == '__main__':
  rospy.init_node('ardrone_square_node_ex')
  ArdroneSquareMoveClass()
  rospy.spin()

    