#! /usr/bin/env python

import rospy                           # Import the Python library for ROS
from geometry_msgs.msg import Twist    # Import the Twist message from the geometry_msgs package

rospy.init_node('topic_publisher')     # Initiate a Node named 'topic_publisher'
pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)    
# Create a Publisher object, that will publish on the /cmd_vel topic
# messages of type Twist

rate = rospy.Rate(2)   # Set a publish rate of 2 Hz
var = Twist()  # Create a var of type Twist

 # Initialize 'var' variable not necessary here              
#var.linear.x = 0.00              
#var.angular.z = 0.00

while not rospy.is_shutdown():  # Create a loop that will go until someone stops the program execution
  pub.publish(var)  # Publish the message within the 'count' variable
  var.linear.x = 0.8   # Set the value for 'linear and angular' variable components
  var.angular.z = 0.4
  rate.sleep()   # Make sure the publish rate maintains at 2 Hz